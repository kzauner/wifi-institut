(function(){
    // Alle Löschen Buttons ermitteln
    let btnDelete = $('.btn-delete');

    // jedem Löschen Button ein Click Event hinzufügen
    btnDelete.click(function(e) {
        let deleteOk = confirm('Soll dieser User gelöscht werden?');
        if (!deleteOk) {
            return false;
        }
        // $(this) -> der geklickte Button; wir holen uns die aufzurufende URL. $(this) erzeugt ein jQuery Objekt.
        let button = $(this);
        let url = button.data('url');
        // die eben ermittele Adresse per AJAX aufrufen
        $.get(url, function(data){
            if(data === '1') {
                // Bei Erfolg die Zeile aus der Tabelle löschen
                // geklickter_button.nächst_höheres_tr. löschen();
                button.closest('tr').remove();
            }
            else {
                alert ('Der Datensatz konnte nicht gelöscht werden!');
            }
        });
    });
    

    // per AJAX die delete URL aufrufen

    // Bei Erfolg die Zeile aus der Tabelle löschen
}());;