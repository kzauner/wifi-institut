<?php
namespace Controller;
use \Template;

class LoginController {
    public function index($f3, $params){
        $f3->set('pageTitle', 'Login');
        $f3->set('mainHeading', 'Login');
        $f3->set('content', '/views/content/login.html');

        // Template ausgeben
        echo Template::instance()->render('views/index.html');
    }
}