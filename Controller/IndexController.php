<?php
namespace Controller;
use \Template;

class IndexController {
    public function index($f3, $params){
        // https://fatfreeframework.com/3.6/framework-variables
        // diese 3 Variablen müssen immer gesetzt werden
        $f3->set('pageTitle', 'Home');
        $f3->set('mainHeading', 'Willkommen im Institut');
        $f3->set('content', '/views/content/home.html');

        // Template ausgeben
        echo Template::instance()->render('views/index.html');
    }
}