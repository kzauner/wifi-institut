<?php
namespace Controller;

use Models\CourseModel;
use \Template;

class CoursesController {
    public function index($f3, $params){
        $cm = new \Models\CourseModel;
        $f3->set('courses', $cm->courses());
   
        $f3->set('pageTitle', 'Kursübersicht');
        $f3->set('mainHeading', 'Kursübersicht');
        $f3->set('content', '/views/content/courses.html');

        // Template ausgeben
        echo Template::instance()->render('views/index.html');
    }

    public function store($f3, $params){

        $formSent = false;
        $formErrors = [];
        $errorMsg = '';
       
        // Formular wurde geschickt
        if (!empty($_POST)) {
            $formSent = true;
        }
        // GUMP 
        $gump = new \GUMP('de');
        $_POST = $gump->sanitize($_POST); // You don't have to sanitize, but it's safest to do so.
        $gump->validation_rules(array(
            'title' => 'required|max_len,255',
            'description' => 'required',
            'trainer' => 'required',
            'room' => 'required',
            'kursstart' => 'required',
            'time_start' => 'required',
            'time_end' => 'required'
        ));
        

        $validData = $gump->run($_POST);
        if($validData === false) {
            $errors = $gump->get_errors_array();
            $f3->set('errors', $errors);
            $f3->set('values', $_POST);
        } 
        else {
            $cm = new \Models\CourseModel();
    
            $isStored = $cm->storeCourse($validData['title'], $validData['description'], $validData['trainer'], $validData['room'], $validData['kursstart'], $validData['time_start'], $validData['time_end']);
            if ($isStored === true) {
                $f3->set('alertSuccess', 'Neuer Kurs erfolgreich angelegt!');
            }
            else {
                $f3->set('alertError', 'Fehler! Kurs konnte nicht angelegt werden!');
            }
        }

        $f3->set('pageTitle', 'Neuer Kurs');
        $f3->set('mainHeading', 'Neuen Kurs anlegen');
        $f3->set('content', '/views/content/courses-store.html');

        // Template ausgeben
        echo Template::instance()->render('views/index.html');
    }
}