<?php
namespace Controller;

use Models\UserModel;
use \Template;

class UserController {
    public function index($f3, $params){
        // DB Logik
        $um = new \Models\UserModel();
        $f3->set('users',$um->users());

        /* 
            Über die F3 Variable jScripts können anhand eines Arrays die zu ladenden JavaScript Dateien als Pfad angegeben werden.
            Beispiel: 
            $f3->set('jScripts', ['js/script1.js', 'js/script2.js']);
        */
        $f3->set('jScripts', ['/js/user.js']);
        $f3->set('pageTitle', 'User Übersicht');
        $f3->set('mainHeading', 'User Übersicht');
        $f3->set('content', '/views/content/user.html');

        // Template ausgeben
        echo Template::instance()->render('/views/index.html');
    }
    /**
     * Einzelnen User Datensatz anzeigen
     *
     * @param Object $f3
     * @param array $params
     * @return void
     */
    public function show($f3, $params) {
        $uid = $params['uid']; 
        
        if (!filter_var($uid, FILTER_VALIDATE_INT)) {
            $user = [];
        }
        else {
            $um = new \Models\UserModel();
            $user = $um->user($uid);
        }
       
        /* 
            Dem Template die User DAten bekannt geben.
            Über die $f3->set MEthode kommunziere ich mit dem Template. Alle über $f3->set gesetzten Werte
            stehen mir zur Verfügung,
        */

        $f3->set('user', $user);
        $f3->set('pageTitle', 'User ' . $user['email']);
        $f3->set('mainHeading', 'User ' . $user['email']);
        $f3->set('content', '/views/content/user-show.html');
        
        echo Template::instance()->render('/views/index.html');

    }
    /**
     * Neuen User anlegen, wird für POST & GET verwendet
     *
     * @param Object $f3
     * @param array $params
     * @return void
     */
    public function store($f3, $params) {
        $formSent = false;
        $formErrors = [];
        $errorMsg = '';
       
        // Formular wurde geschickt
        if (!empty($_POST)) {
            $formSent = true;
        }

        // GUMP 
        $gump = new \GUMP('de');
        $_POST = $gump->sanitize($_POST); // You don't have to sanitize, but it's safest to do so.
        $gump->validation_rules(array(
            'email' => 'required|valid_email|max_len,255',
            'password' => 'required|alpha_numeric|max_len,255|min_len,8',
        ));

        $validData = $gump->run($_POST);

        if($validData === false) {
            $errors = $gump->get_errors_array();
            $f3->set('errors', $errors);
            $f3->set('values', $_POST);
        } 
        else {
            $um = new \Models\UserModel();
            $isStored = $um->storeUser($validData['email'], $validData['password']);
            if ($isStored === true) {
                $f3->set('alertSuccess', 'Neuer User erfolgreich eingetragen!');
            }
            else {
                $f3->set('alertError', 'Fehler! User konnte nicht eingetragen werden!');
            }
        }

        $f3->set('pageTitle', 'Neuer User');
        $f3->set('mainHeading', 'Neuer User');
        $f3->set('content', '/views/content/user-store.html');

        echo Template::instance()->render('/views/index.html');
    }

    /**
     * User editieren, wird für POST & GET verwendet
     *
     * @param Object $f3
     * @param array $params
     * @return void
     */
    public function edit($f3, $params) {
    $uid = $params['uid'];
    if (!filter_var($uid, FILTER_VALIDATE_INT)) {
        $values = [];
    }
    else {
        $um = new \Models\UserModel();
        $values = $um->user($uid);
    }

    // Formular wurde geschickt
    if (!empty($_POST)) {
        $gump = new \GUMP('de');

        $gump->validation_rules(array(
            'email' => 'required|valid_email|max_len,255',
            'password' => 'required|alpha_numeric|max_len,255|min_len,8'
        ));

        $validData = $gump->run($_POST);

        if ($validData === false) {
            $errors = $gump->get_errors_array();
            $f3->set('errors', $errors);
            /* 
                Bei Fehler überschreiben wir die Werte aus der DB mit den Werten aus dem Formular,
                da diese einen höheren Stellenwert haben.
            */
            $values = $_POST;
        }
        else {
            $um = new \Models\UserModel();
            $isStored = $um->updateUser($validData['email'], $validData['password'], $uid);
            $values = $validData; // damit nicht die alten Daten angezeigt werden
            
            if ($isStored === true) {
                $f3->set('alertSuccess', 'User erfolgreich aktualisiert.');
            }
            else {
                $f3->set('alertError', 'Fehler! User konnte nicht aktualisiert werden.');
            }
        }
    }

        $f3->set('values', $values);

        $f3->set('pageTitle', 'User bearbeiten');
        $f3->set('mainHeading', 'User bearbeiten');
        $f3->set('content', '/views/content/user-edit.html');

        echo Template::instance()->render('/views/index.html');
    }

    public function delete($f3, $params) {
        $uid = $params['uid']; 
        
        if (!filter_var($uid, FILTER_VALIDATE_INT)) {
            echo 'Error: User konnte nicht gelöscht werden.';
        }
        else {
           $um = new UserModel();
           $isDeleted = $um->deleteUser($uid);

           if ($isDeleted === true) {
               $f3->set('alertSuccess', 'User erfolgreich gelöscht.');
           }
           else {
               $f3->set('alertError', 'Fehler! User konnte nicht gelöscht werden.');
           }
            // Bei Erfolg wird 1 zurück gegeben
            echo '1';
        }
    }
}