<?php
// Autoloader
require_once 'vendor/autoload.php';

// Fatfree Objekt erzeugen
$f3 = \Base::instance();
// var_dump($f3);

/* 
    DATABASE -- nicht mehr notwendig, da es sich im Model befindet!
    https://fatfreeframework.com/3.6/databases
    $f3->set('DB', new DB\SQL(
        'mysql:host=localhost; dbname=institut',
        'root',
        ''
    ));
    var_dump($f3->get('DB')); */

// ROUTES
// nach Erstellen des $f3 Objektes können wir die Routes importieren
require 'routes.php';
// App starten
$f3->run();
