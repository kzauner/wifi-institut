<?php
// https://fatfreeframework.com/3.6/routing-engine
/* $f3->route('GET /', function($f3) {
    echo '<h1>Welcome Home</h1>';
}); */

$f3->route('GET /', 'Controller\IndexController->index');

$f3->route('GET /user', 'Controller\UserController->index');
// https://fatfreeframework.com/3.6/routing-engine#RoutesandTokens
$f3->route('GET /user/@uid', 'Controller\UserController->show');

$f3->route('GET /user/new', 'Controller\UserController->store'); // view ist per GET
$f3->route('POST /user/new', 'Controller\UserController->store'); // absenden ist per POST

$f3->route('GET /user/@uid/edit', 'Controller\UserController->edit');
$f3->route('POST /user/@uid/edit', 'Controller\UserController->edit');

$f3->route('GET /user/@uid/delete', 'Controller\UserController->delete');
/* 
    $f3->route('GET /product/@id', function($f3, $params){
    print_r($params);
    echo 'Jipie, die ID: ' .$params['id'];
    }); 
*/

$f3->route('GET /login', 'Controller\LoginController->index');

// COURSES routes

$f3->route('GET /courses', 'Controller\CoursesController->index');

$f3->route('GET /courses/new', 'Controller\CoursesController->store');
$f3->route('POST /courses/new', 'Controller\CoursesController->store');
