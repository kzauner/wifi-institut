<?php
namespace Models;

class UserModel extends Model {
    /**
     * Alle User ermitteln
     *
     * @return array
     */
    public function users() : array {
        return $this->db->exec('SELECT * FROM user');
    }
    /**
     * Einen einzelnen User ermitteln
     *
     * @param integer $id
     * @return array
     */
    public function user(int $id) : array {
        $user = $this->db->exec('SELECT * FROM user WHERE id=?', $id);

        if (count($user) === 0) {
            return [];
        }
        return $user[0];
    }

    /**
     * Neuen User anlegen
     *
     * @param string $email
     * @param string $pw
     * @return boolean
     */
    public function storeUser(string $email, string $pw) : bool {
        // INSERT INTO user (email, password) VALUES ('bla@bla.at', '6789')
        $isStored = $this->db->exec('INSERT INTO user (email, password) VALUES (?, ?)', [$email, $pw]);
        return $isStored;
    }

    public function updateUser(string $email, string $pw, int $id) : bool {
        $isStored = $this->db->exec('UPDATE user SET email = ?, password = ? WHERE id = ?', [$email, $pw, $id]);
        return $isStored;
    }

    public function deleteUser(int $id) : bool {
        $isDeleted = $this->db->exec('DELETE FROM user WHERE id = ?', $id);
        return $isDeleted;
    }
}