<?php
namespace Models;

abstract class Model {
    protected $db;

    public function __construct() {
        // DB Verbindung am Klassen Attribut sichern
        $this->db = new \DB\SQL(
            'mysql:host=localhost; port=3306; dbname=institut',
            'root',
            ''
        );
    }
}