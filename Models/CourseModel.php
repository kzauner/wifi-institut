<?php

namespace Models;

class CourseModel extends Model{

    public function courses() : array {
        return $this->db->exec('SELECT courses.*, trainer.name as tName, rooms.number as rNumber FROM `courses` LEFT JOIN trainer ON courses.trainer_id = trainer.id LEFT OUTER JOIN rooms ON courses.room_id = rooms.id');
    }

    public function storeCourse(string $title, string $desc, string $trainer, string $room, string $date, string $timeStart, string $timeEnd) : bool {
        $isStored = $this->db->exec('INSERT INTO courses (title, description, trainer_id, room_id, date, time_start, time_end) VALUES (?, ?, ?, ?, ?, ?, ?)', [$title, $desc, $trainer, $room, $date, $timeStart, $timeEnd]);
        return $isStored;
    }
}